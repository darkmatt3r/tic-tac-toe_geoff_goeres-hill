﻿// <summary>
// This class manages the game board and game win conditions.
// </summary>

using System;
using UnityEngine;
using UnityEngine.UI;

public class GameBoard : MonoBehaviour
{
    private static GameBoard Instance { get; set; }

    private static int rows = 3;
    private static int cols = 3;

    [SerializeField] private GameObject cellPrefab;

    private BoardSize boardSize;
    private Cell[,] cells = new Cell[rows, cols];
    private Cell lastChangedCell;
    private CellState currentPlayer;
    private GameObject container;

    private event Action<CellState, int, int> OnBoardChange;

    public enum BoardSize
    {
        Three,
        Four,
    }

    private void Awake()
    {
        Instance = this;
    }

    public void Init(Action<CellState, int, int> onBoardChangeAction)
    {
        CreateCells();
        SetPlayer(CellState.Empty);
        OnBoardChange = onBoardChangeAction;
    }

    public void SetPlayer(CellState cellState)
    {
        currentPlayer = cellState;

        if (cellState != CellState.Empty)
            Debug.Log(currentPlayer + "'s turn.");
    }

    public void SetCell(CellState cellState, int row, int col)
    {
        SetPlayer(cellState);
        OnCellClick(cells[row, col]);
    }

    public void SetBoardSize(BoardSize newBoardSize)
    {
        this.boardSize = newBoardSize;
        switch (newBoardSize)
        {
            case BoardSize.Three:
                rows = cols = 3;
                break;
            case BoardSize.Four:
                rows = cols = 4;
                break;
            default:
                rows = cols = 3;
                break;
        }

        cells = new Cell[rows, cols];
        CreateCells();

        Debug.Log($"Board Size {newBoardSize}");
    }

    public void Clear()
    {
        for (var row = 0; row < rows; row++)
        {
            for (var col = 0; col < cols; col++)
            {
                cells[row, col].Clear();
            }
        }

        lastChangedCell = null;
    }

    public bool IsDraw()
    {
        for (var row = 0; row < rows; row++)
        {
            for (var col = 0; col < cols; col++)
            {
                if (cells[row, col].Content == CellState.Empty)
                {
                    return false;
                }
            }
        }

        return true;
    }

    public bool HasWon(CellState cellState)
    {
        if (lastChangedCell == null)
        {
            return false;
        }

        return HasWonInTheRow(cellState)
               || HasWonInTheColumn(cellState)
               || HasWonInTheDiagonal(cellState)
               || HasWonInTheOppositeDiagonal(cellState);
    }

    private bool HasWonInTheRow(CellState cellState)
    {
        var row = lastChangedCell.Row;

        switch (boardSize)
        {
            case BoardSize.Three:
                return cells[row, 0].HasState(cellState)
                       && cells[row, 1].HasState(cellState)
                       && cells[row, 2].HasState(cellState);
            case BoardSize.Four:
                return cells[row, 0].HasState(cellState)
                       && cells[row, 1].HasState(cellState)
                       && cells[row, 2].HasState(cellState)
                       && cells[row, 3].HasState(cellState);
            default:
                return false;
        }
    }

    private bool HasWonInTheColumn(CellState cellState)
    {
        var col = lastChangedCell.Col;

        switch (boardSize)
        {
            case BoardSize.Three:
                return cells[0, col].HasState(cellState)
                       && cells[1, col].HasState(cellState)
                       && cells[2, col].HasState(cellState);
            case BoardSize.Four:
                return cells[0, col].HasState(cellState)
                       && cells[1, col].HasState(cellState)
                       && cells[2, col].HasState(cellState)
                       && cells[3, col].HasState(cellState);
            default:
                return false;
        }
    }

    private bool HasWonInTheDiagonal(CellState cellState)
    {
        switch (boardSize)
        {
            case BoardSize.Three:
                return cells[0, 0].HasState(cellState)
                       && cells[1, 1].HasState(cellState)
                       && cells[2, 2].HasState(cellState);
            case BoardSize.Four:
                return cells[0, 0].HasState(cellState)
                       && cells[1, 1].HasState(cellState)
                       && cells[2, 2].HasState(cellState)
                       && cells[3, 3].HasState(cellState);
            default:
                return false;
        }
    }

    private bool HasWonInTheOppositeDiagonal(CellState cellState)
    {
        switch (boardSize)
        {
            case BoardSize.Three:
                return cells[0, 2].HasState(cellState)
                       && cells[1, 1].HasState(cellState)
                       && cells[2, 0].HasState(cellState);
            case BoardSize.Four:
                return cells[0, 3].HasState(cellState)
                       && cells[1, 2].HasState(cellState)
                       && cells[2, 1].HasState(cellState)
                       && cells[3, 0].HasState(cellState);
            default:
                return false;
        }
    }

    private void OnCellClick(Cell cell)
    {
        if (currentPlayer == CellState.Empty || !cell.IsEmpty) return;

        cell.Set(currentPlayer);
        lastChangedCell = cell;

        OnBoardChange?.Invoke(currentPlayer, cell.Row, cell.Col);
    }

    private void CreateCells()
    {
        if (container != null)
            Destroy(container);

        container = new GameObject("GridSpaces", typeof(RectTransform));

        container.transform.SetParent(transform, false);
        container.transform.localPosition = new Vector3(0f, 0f, 0f);

        var rt = container.GetComponent<RectTransform>();
        var gridSize = boardSize == BoardSize.Three ? new Vector2(770, 770) : new Vector2(1030, 1030);
        rt.sizeDelta = gridSize;

        for (var row = 0; row < rows; row++)
        {
            for (var col = 0; col < cols; col++)
            {
                var go = Instantiate(cellPrefab, container.transform);
                var cell = go.GetComponent<Cell>();
                cell.Init(row, col, OnCellClick);

                cells[row, col] = cell;
            }
        }

        var gridLayoutGroup = container.gameObject.AddComponent<GridLayoutGroup>();
        gridLayoutGroup.cellSize = new Vector2(250, 250);
        gridLayoutGroup.spacing = new Vector2(10, 10);
        gridLayoutGroup.startCorner = GridLayoutGroup.Corner.LowerLeft;
        gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedRowCount;
        gridLayoutGroup.constraintCount = rows;
    }
}