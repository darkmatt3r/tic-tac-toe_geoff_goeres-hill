﻿// <summary>
// This class manages the player icon selection.
// </summary>

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class IconSelection : MonoBehaviour
{
    public static IconSelection Instance { get; private set; }

    [SerializeField] private Sprite[] sprites;
    [SerializeField] private RectTransform[] panels;

    [SerializeField] private Button spriteSelectionButtonPrefab;
    [SerializeField] private Color alreadySelectedColor = Color.grey;

    private readonly int[] playerSprites = {0, 1};
    private Dictionary<int, List<Button>> buttonsByPlayer = new Dictionary<int, List<Button>>();

    private void Awake()
    {
        Instance = this;

        buttonsByPlayer = new Dictionary<int, List<Button>>();

        for (var playerIndex = 0; playerIndex < panels.Length; playerIndex++)
        {
            var panel = panels[playerIndex];
            for (var spriteIndex = 0; spriteIndex < sprites.Length; spriteIndex++)
            {
                var button = Instantiate(spriteSelectionButtonPrefab, panel, true);
                var index = playerIndex;
                var index1 = spriteIndex;
                button.onClick.AddListener(() => SelectSpriteForPlayer(index, index1));
                button.image.sprite = sprites[spriteIndex];

                if (buttonsByPlayer.ContainsKey(playerIndex) == false)
                    buttonsByPlayer.Add(playerIndex, new List<Button>());

                buttonsByPlayer[playerIndex].Add(button);
            }
        }

        RefreshButtons();
    }

    public Sprite GetSprite(int playerIndex)
    {
        var spriteIndex = playerSprites[playerIndex];
        return sprites[spriteIndex];
    }

    private void RefreshButtons()
    {
        for (var playerIndex = 0; playerIndex < panels.Length; playerIndex++)
        {
            for (var spriteIndex = 0; spriteIndex < sprites.Length; spriteIndex++)
            {
                var button = buttonsByPlayer[playerIndex][spriteIndex];
                var isSelected = playerSprites[playerIndex] == spriteIndex;
                button.image.color = isSelected ? Color.white : alreadySelectedColor;
                button.interactable = CanSelectSprite(spriteIndex);
            }
        }
    }

    private void SelectSpriteForPlayer(int playerIndex, int spriteIndex)
    {
        if (CanSelectSprite(spriteIndex) == false)
            return;

        playerSprites[playerIndex] = spriteIndex;

        RefreshButtons();
    }

    private bool CanSelectSprite(int spriteIndex)
    {
        return playerSprites.All(t => t != spriteIndex);
    }
}