﻿// <summary>
// This class manages the game results UI.
// </summary>

using UnityEngine;
using UnityEngine.UI;

public class GameResults : BasePanel
{
    [SerializeField] private Text gameResults;
    [SerializeField] private Button buttonNewGame;
    [SerializeField] private Button buttonQuitToMenu;

    public void OnQuitToMenu()
    {
        GameManager.Instance.Quit();
    }

    public void OnNewGame()
    {
        GameManager.Instance.NewGame();
    }

    protected override void Show()
    {
        base.Show();
        buttonNewGame.gameObject.SetActive(true);
        buttonQuitToMenu.gameObject.SetActive(true);
    }

    protected override void OnNewGame(GameManager game)
    {
        Hide();
    }

    protected override void OnGameResults(GameManager game)
    {
        OnGameEnd(game);
    }

    protected override void OnGameQuit(GameManager game)
    {
        Hide();
    }

    private void OnGameEnd(GameManager game)
    {
        switch (game.CurrentState)
        {
            case GameState.X_Win:
                gameResults.text = "PLAYER 1 WINS!";
                break;
            case GameState.O_Win:
                gameResults.text = "PLAYER 2 WINS!";
                break;
            case GameState.Draw:
                gameResults.text = "DRAW!";
                break;
        }

        Show();
    }
}