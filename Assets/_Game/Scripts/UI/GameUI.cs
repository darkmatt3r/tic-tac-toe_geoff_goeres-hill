﻿// <summary>
// This class manages the in-game UI.
// </summary>

using UnityEngine;
using UnityEngine.UI;

public class GameUI : BasePanel
{
    [SerializeField] private Text player1Score;
    [SerializeField] private Text player2Score;
    [SerializeField] private Button buttonBack;
    [SerializeField] private Button buttonRestart;

    public void OnBack()
    {
        GameManager.Instance.Quit();
    }

    public void OnRestart()
    {
        GameManager.Instance.NewGame();
    }

    protected override void Show()
    {
        base.Show();
        buttonBack.gameObject.SetActive(true);
        buttonRestart.gameObject.SetActive(true);
    }

    protected override void OnNewGame(GameManager game)
    {
        Show();
    }

    protected override void OnGameResults(GameManager game)
    {
        UpdateGameScore(game);
    }

    protected override void OnGameQuit(GameManager game)
    {
        Hide();
    }

    private void UpdateGameScore(GameManager game)
    {
        player1Score.text = GameManager.Player1.ToString();
        player2Score.text = GameManager.Player2.ToString();
    }
}