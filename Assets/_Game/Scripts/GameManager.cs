﻿// <summary>
// This class manages the game setup, game states, and game events.
// It also keeps a list of the player move history.
// </summary>

using System;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Menu,
    InPlay,
    Draw,
    X_Win,
    O_Win
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public static Player Player1 { get; private set; }
    public static Player Player2 { get; private set; }

    public static event Action<GameManager> OnNewGame;
    public static event Action<GameManager> OnGameResults;
    public static event Action<GameManager> OnGameQuit;

    [SerializeField] private GameBoard gameBoard;

    private List<PlayerMove> playerMoves = new List<PlayerMove>();

    public GameState CurrentState { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Reset();

        CurrentState = GameState.Menu;

        gameBoard.Init(OnBoardChange);
        gameBoard.SetPlayer(CellState.Empty);
        gameBoard.gameObject.SetActive(false);
    }

    public void Play()
    {
        NewGame();
    }

    public void NewGame()
    {
        gameBoard.gameObject.SetActive(true);
        gameBoard.Clear();
        gameBoard.SetPlayer(CellState.X);
        CurrentState = GameState.InPlay;

        OnNewGame?.Invoke(this);
    }

    public void Quit()
    {
        CurrentState = GameState.Menu;
        HideBoard();

        OnGameQuit?.Invoke(this);
    }

    private void Reset()
    {
        Player1 = new Player("Player 1", 0, CellState.X);
        Player2 = new Player("Player 2", 0, CellState.O);
    }

    private void HideBoard()
    {
        gameBoard.gameObject.SetActive(false);
    }

    private void OnBoardChange(CellState player, int row, int col)
    {
        var playerMove = new PlayerMove(player, row, col);
        playerMoves.Add(playerMove);
        Debug.Log(playerMove);
        var nextPlayer = CellState.Empty;

        if (gameBoard.HasWon(player))
        {
            switch (player)
            {
                case CellState.X:
                    CurrentState = GameState.X_Win;
                    Player1.Score++;
                    Debug.Log("Player 1 wins!");
                    break;
                case CellState.O:
                    CurrentState = GameState.O_Win;
                    Player2.Score++;
                    Debug.Log("Player 2 wins!");
                    break;
            }

            OnGameResults?.Invoke(this);
        }
        else if (gameBoard.IsDraw())
        {
            CurrentState = GameState.Draw;
            Debug.Log("Game is a draw!");

            OnGameResults?.Invoke(this);
        }
        else
        {
            nextPlayer = player == CellState.X ? CellState.O : CellState.X;
        }

        gameBoard.SetPlayer(nextPlayer);
    }
}